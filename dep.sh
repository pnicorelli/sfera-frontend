#/bin/bash

if [ "$EUID" -ne 0 ]
then
  echo "ERR: Please run as root"
  exit 1
fi


apt update
apt install lighttpd php7.0-fpm php-cgi

WORKPLACE=$(mktemp -d)
trap "rm -rf $WORKPLACE" EXIT

cd $WORKPLACE

# installing composer
wget https://getcomposer.org/installer
mv composer.phar /usr/local/bin/composer


# setup lighttpd to work with php
sudo lighty-enable-mod fastcgi 
sudo lighty-enable-mod fastcgi-php
