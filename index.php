<?php
require_once __DIR__ . '/vendor/autoload.php';

$send ="";
if( $_REQUEST["action"] == "sendCommand"){
	$server = "localhost";
	$port = 1883;
	$username = "";
	$password = "";
	$client_id =  uniqid();

	$mqtt = new Bluerhinos\phpMQTT($server, $port, $client_id);
	if ($mqtt->connect(true, NULL, $username, $password)) {
		$mqtt->publish("local/technician", $_REQUEST["lights"]);
		$mqtt->close();
		$send ="OK";
	} else {
	  $send ="KO";
	}
}
?>
<html>

<body>
	<?php echo $send;?>
	<form method="GET">
		<input type="radio" id="lights_on" name="lights" value="lights_on" />ON<br />
		<input type="radio" id="lights_off" name="lights" value="lights_off" />OFF<br />
		<input type="hidden" name="action" value="sendCommand" />
		<button>OK</button>
	</form>
</body>
</html>
